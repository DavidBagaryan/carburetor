import threading, queue, time


def washer(_dishes, _dish_queue):
    for dish in _dishes:
        print('Washing', dish)
        time.sleep(3)
        _dish_queue.put(dish)


def dryer(_dish_queue):
    while True:
        dish = _dish_queue.get()
        print('Drying', dish, '\n')
        time.sleep(6)
        _dish_queue.task_done()


dish_queue = queue.Queue()

for n in range(3):
    dryer_thread = threading.Thread(target=dryer, args=(dish_queue,))
    dryer_thread.start()

dishes = ['salad', 'bread', 'entree', 'dessert']
washer(dishes, dish_queue)

dish_queue.join()
