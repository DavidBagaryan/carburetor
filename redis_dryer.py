import redis

redis_conn = redis.Redis()

print('\nstart drying', '\n=========')

while True:
    msg = redis_conn.blpop('dishes')
    if not msg:
        break

    val = msg[1].decode()
    if val == 'quit':
        break

    print('dried', val, '\n=======')

print('dishes are dried')
