from twisted.internet import protocol, reactor


class KnockClient(protocol.Protocol):
    def connectionMade(self):
        self.transport.write('knock knock'.encode())

    def dataReceived(self, data):
        data = data.decode()
        if data.startswith("who's there?"):
            response = 'your client MFK!'
            self.transport.write(response.encode())
        else:
            self.transport.loseConnection()
            reactor.stop()


class KnockFactory(protocol.ClientFactory):
    protocol = KnockClient


def main():
    f = KnockFactory()
    reactor.connectTCP('localhost', 8008, f)
    reactor.run()


if __name__ == '__main__':
    main()
