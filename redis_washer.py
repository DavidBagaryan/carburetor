import redis

redis_conn = redis.Redis()

print('\nstart washing', '\n=========')

dishes = ['salad', 'bread', 'entree', 'dessert']
for dish in dishes:
    msg = dish.encode()
    redis_conn.rpush('dishes', msg)
    print('washed', dish)

redis_conn.rpush('dishes', 'quit')

print('washer is done')
