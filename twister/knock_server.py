from twisted.internet import protocol, reactor


class Knock(protocol.Protocol):
    def dataReceived(self, data):
        data = data.decode()
        print(f'client: {data}')

        response = "who's there?" if data.startswith('knock knock') else f'{data}, who?'
        print(f'server: {response}')

        self.transport.write(response.encode())


class KnockFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Knock()


reactor.listenTCP(8008, KnockFactory())
reactor.run()
